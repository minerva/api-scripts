##################################################
## MINERVA API version: 14.0
## Script purpose: An example of MINERVA API calls adding curators form the file users.txt
## Date: 23/03/2020
## Author: Piort Gawron (piotr.gawron@uni.lu)
## MANUAL: https://minerva.pages.uni.lu/doc/api/14.0/users/
##################################################

### A convenience function to handle the text files and the server repsonses
import csv
import requests

session = requests.Session()

### INPUT YOUR CREDENTIALS TO LOGIN to PD map instance
login = "  "
password = "  "
api_url = "https://pdmap.uni.lu/minerva/api/"


# LOGIN to PD map instance with your credentials
login_request = session.post(api_url+"/doLogin", data = {'login': login, 'password': password})

print(login_request.text)
print(session.cookies.get_dict())


# READ USER LIST from the users.txt
with open("users.txt") as tsv:
  for line in csv.reader(tsv, delimiter="\t"):
    user_email = line[0]
    user_name = line[1]
    user_surname = line[2]
    user_login = line[3]
    user_password = line[4]
    print ("User: ",user_login)
    # GET USER DETAILS (check if user_login already exists in MINERVA)
    user_request = session.get(api_url+"/users/"+user_login)

    if user_request.status_code != 200:  # if user does not exist
      print("Creating")
      # CREATE NEW USER with details provided in the file users.txt
      create_user_request = session.post(api_url+"/users/"+user_login,
                                         data = {'name':user_name, 'surname': user_surname, 'email':user_email, 'password':user_password})
      print(create_user_request.status_code)

    else:
      print("User Exists")
      # GET existing USER details
      specific_user_data = session.get(api_url+"/users/"+user_login)
      print(specific_user_data.text)

      # UPDATE USER user_login details as per the file users.txt
      update_user_request = session.patch(api_url+"/users/"+user_login,
                                          data = '{"user":{"name":"'+user_name+'", "surname": "'+user_surname+'", "email":"'+user_email+
                                                 '", "password":"'+user_password+'"}}')
      print(update_user_request.status_code)

    # MODIFY USER PRIVILEGES from 'user' onto 'curator'
    update_privileges_request = session.patch(api_url+"/users/"+user_login+
                                              ":updatePrivileges", data = '{"privileges":{"IS_CURATOR":true, "IS_ADMIN":false}}')
    print(update_privileges_request.status_code)

