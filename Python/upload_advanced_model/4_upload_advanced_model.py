##################################################
## MINERVA API version: 14.0
## Script purpose: An example of MINERVA API calls uploading zipped project
## Date: 23/03/2020
## Author: Piort Gawron (piotr.gawron@uni.lu)
## MANUAL: https://minerva.pages.uni.lu/doc/api/14.0/projects/
##################################################


import requests
import os
import json

session = requests.Session()

### INPUT YOUR CREDENTIALS TO LOGIN to PD map instance
login = "  "
password = "  "
api_url = "https://pdmap.uni.lu/minerva/api/"
map_file = "example_advanced.zip"

# LOGIN to PD map instance with your credentials
login_request = session.post(api_url+"/doLogin", data = {'login': login, 'password': password})
print(login_request.text)
print(session.cookies.get_dict())


# UPLOAD FILE TO THE SYSTEM
stat_info = os.stat(map_file)
with open(map_file, mode ="rb") as f: file_content = f.read()

# Before creating a new project in MINERVA, source file 'map_file' must be uploaded to the instance (MANUAL: https://minerva.pages.uni.lu/doc/api/14.0/files/)
# Allocate memory in the system for 'map_file', which length is 'stat_info.st_size' bytes
create_file_request = session.post(api_url+"/files/", data = {'filename': map_file, 'length': stat_info.st_size})

# Get the information about the uploaded file: the 'id' is necessary to upload the file's content.
content  = json.loads(create_file_request.text)

file_id = content["id"]
# Upload file's content to the instance
upload_content_request = session.post(api_url+"/files/"+str(file_id)+":uploadContent", data = file_content)


# CREATE PROJECT FROM FILE
project_id = "adv_"+str(file_id)

# DEFINE PROJECT PARAMETERS (what is what in the zip file)
data = [
  ('projectId', project_id),
  ('name', ''),
  ('parser', 'lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser'),
  ('file-id', str(file_id)),
  ('auto-resize', 'true'),
  ('cache', 'false'),
  ('notify-email', ''),
  ('disease', ''),
  ('version', ''),
  ('organism', ''),
  ('mapCanvasType', 'OPEN_LAYERS'),
  ('sbgn', 'false'),
  ('semantic-zoom', 'false'),
  ('annotate', 'false'),
  ('verify-annotations', 'false'),
  ('zip-entries[0][_type]', 'MAP'),
  ('zip-entries[0][_filename]', 'example_advanced_map.xml'),
  ('zip-entries[0][_data][root]', 'true'),
  ('zip-entries[0][_data][name]', 'example_advanced_map'),
  ('zip-entries[0][_data][type][id]', 'UNKNOWN'),
  ('zip-entries[0][_data][type][name]', 'Unknown'),
  ('zip-entries[1][_type]', 'IMAGE'),
  ('zip-entries[1][_filename]', 'images/coords.txt'),
  ('zip-entries[2][_type]', 'IMAGE'),
  ('zip-entries[2][_filename]', 'images/overview_sub.png'),
  ('zip-entries[3][_type]', 'IMAGE'),
  ('zip-entries[3][_filename]', 'images/overview_main.png'),
  ('zip-entries[4][_type]', 'OVERLAY'),
  ('zip-entries[4][_filename]', 'overlays/example_upload_basic.txt'),
  ('zip-entries[4][_data][name]', 'Advanced example'),
  ('zip-entries[4][_data][description]', 'Advanced example of custom layout'),
  ('zip-entries[5][_type]', 'MAP'),
  ('zip-entries[5][_filename]', 'submaps/mapping.xml'),
  ('zip-entries[5][_data][mapping]', 'true'),
  ('zip-entries[5][_data][name]', 'mapping'),
  ('zip-entries[5][_data][type][id]', 'UNKNOWN'),
  ('zip-entries[5][_data][type][name]', 'Unknown'),
  ('zip-entries[6][_type]', 'MAP'),
  ('zip-entries[6][_filename]', 'submaps/example_submap.xml'),
  ('zip-entries[6][_data][name]', 'example_submap'),
  ('zip-entries[6][_data][type][id]', 'UNKNOWN'),
  ('zip-entries[6][_data][type][name]', 'Unknown'),
]

# AND CREATE A PROJECT
create_map_request = session.post(api_url+"/projects/"+project_id, data = data)

print(create_map_request.text)
