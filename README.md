# API scripts

Here we share scripts using MINERVA API. Directories indicate the types of
scripts (R, Python, etc.).  

The table below provides brief description.    


| API version | Directory  |  Name of the script | The script description |
| ----------- | ---------- | ------------------- | ---------------------- |
| 14.0.x | R | API_minervar.R                    | Retrieve and compare UniProt identifiers and interactions across disease maps. |
| 14.0.x | R | API_find_drug_targets.R           | Retrieve HGNC symbols of the elements targetted by a given drug; retrieve drugs targetting elements with a given UniProt. |
| 14.0.x | R | API_retrieve.R                    | Retrieve elements with a given UniProt id and their interactions. |
| 14.0.x | R | API_retrieve_neighbors_of_degs.R  | Retrieve neighbours of 10 most differentially expressed molecules in a given data overlay. |
| 14.0.x | Python | create_accounts              | Add two new users and grant them the privileges as curator. |
| 14.0.x | Python | upload_advanced_model        | Upload a zipped  map (advanced upload). |
| 14.0.x | Python |  upload_data_overlay         | Add a data overlay and a comment to the project. |
| 14.0.x | Python | upload_simple                | Add the new CellDesigner map (basic upload). |
